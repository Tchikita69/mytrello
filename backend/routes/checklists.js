var express = require('express');
var router = express.Router();
var db = require('../dbconf');

router.post('/create', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 256 || req.body.name.length <= 0) {
      res.sendStatus(400);
      return;
    }
    console.log(req.body.board_token);
    db.query('INSERT INTO checklists(name) VALUES ($1) RETURNING id', [req.body.name], function(err, result2) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        db.query('UPDATE cards SET checklists = array_append(checklists, $1) WHERE id=$2', [result2.rows[0].id, req.body.card_id], function(err) {
          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else if (!res.headersSent)
            res.status(201).json({checklist_id: result2.rows[0].id});
        });
      }
    });
  });
});

router.post('/update', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 100 || req.body.name.length <= 0 || !req.body.id)
      res.sendStatus(400);
    if (res.headersSent)
      return;
    var elements = req.body.elements ? req.body.elements.split(',') : [];
    var checked = req.body.checked ? req.body.checked.split(',') : [];
    db.query('UPDATE checklists SET name=$1, elems=$2, checked=$3 WHERE id=$4', [req.body.name, elements, checked, req.body.id], function(err) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  });
});

router.delete('/remove', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var result = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!result.rows.length) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.id) {
    res.sendStatus(400);
    return;
  }
  db.query('DELETE FROM checklists * WHERE id=$1', [req.query.id], function(err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
});

module.exports = router;