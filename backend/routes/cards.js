var express = require('express');
var router = express.Router();
var db = require('../dbconf');

router.post('/create', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 100 || req.body.name.length <= 0 || !req.body.list_id)
      res.sendStatus(400);
    // if (!userInBoard(req.body.list_id, result.rows[0].id)) {
    //   res.sendStatus(401);
    //   return;
    // }
    if (res.headersSent)
      return;
    db.query('INSERT INTO cards(name) VALUES ($1) RETURNING id', [req.body.name], function(err, result2) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.log('list_id: ' + req.body.list_id.toString());
        db.query('UPDATE lists SET cards = array_append(cards, $1) WHERE id=$2', [result2.rows[0].id, req.body.list_id], function(err) {
          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else if (!res.headersSent)
            res.status(201).json({card_id: result2.rows[0].id});
        });
      }
    });
  });
});

router.post('/update', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 100 || req.body.name.length <= 0 || !req.body.id)
      res.sendStatus(400);
    var description = req.body.description ? req.body.description : '';
    var members = req.body.members ? req.body.members.split(',') : [];
    var labels = req.body.labels ? req.body.labels.split(',') : [];
    var checklists = req.body.checklists ? req.body.checklists.split(',') : [];
    if (res.headersSent)
      return;
    db.query('UPDATE cards SET name=$1, description=$2, members=$3, labels=$4, checklists=$5 WHERE id=$6', [req.body.name, description, members, labels, checklists, req.body.id], function(err) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  });
});

router.delete('/remove', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var result = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!result.rows.length) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.id) {
    res.sendStatus(400);
    return;
  }
  db.query('DELETE FROM cards * WHERE id=$1', [req.query.id], function(err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
});

// async function userInBoard(list_id, user_id) {
//   var result = await db.query('SELECT * FROM boards WHERE lists@>$1', [list_id]);
//   if (!result.rows[0].auth_users.includes(user_id))
//     return (false);
//   return (true);
// }

module.exports = router;
