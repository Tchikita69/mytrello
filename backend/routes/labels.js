var express = require('express');
var router = express.Router();
var db = require('../dbconf');

router.post('/create', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var colors = ['red', 'green', 'purple', 'blue', 'yellow', 'orange', 'pink']
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 100 || req.body.name.length <= 0 || !req.body.board_token || !colors.includes(req.body.color.toLowerCase()))
      res.sendStatus(400);
    if (res.headersSent)
      return;
    db.query('INSERT INTO labels(name, color) VALUES ($1, $2) RETURNING id', [req.body.name, req.body.color], function(err, result2) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        console.log(result2);
        console.log(result2.rows)
        db.query('UPDATE boards SET labels = array_append(labels, $1) WHERE token=$2', [result2.rows[0].id, req.body.board_token], function(err) {
          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else if (!res.headersSent)
            res.status(201).json({label_id: result2.rows[0].id});
        });
      }
    });
  });
});

router.delete('/remove', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var result = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!result.rows.length) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.id) {
    res.sendStatus(400);
    return;
  }
  db.query('DELETE FROM labels * WHERE id=$1', [req.query.id], function(err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
});

module.exports = router;
