var express = require('express');
var router = express.Router();
var db = require('../dbconf');
const jwt = require('jsonwebtoken');
const conf = require('../conf');

router.post('/register', function(req, res, next) {
  sqlCheck = "SELECT * FROM users WHERE mail=$1 OR username=$2";
  sqlQuery = "INSERT INTO users(mail, username, password, token) VALUES ($1, $2, $3, $4);";
  if (!req.body.username || !req.body.password || !req.body.mail || req.body.username.length <= 0 || req.body.username.length > 100 || req.body.password.length <= 0 || req.body.password.length > 100 || !req.body.mail.includes('@')) {
    res.status(400).json({error: "Invalid or missing parameter."});
    return;
  }
  db.query(sqlCheck, [req.body.mail, req.body.username], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!result.rows.length) {
      const token = jwt.sign({_id: req.body.username}, conf.secret);
      db.query(sqlQuery, [req.body.mail, req.body.username, req.body.password, token], function(err) {
        if (err) {
          console.log(err);
          res.sendStatus(500);
        } else {
          res.sendStatus(201);
        }
      });
    } else
      res.status(409).json({error: "Username or email already in use."});
  });
});

router.post('/login', async function(req, res) {
  console.log(req.body.username, req.body.password);
  db.query("SELECT * FROM users WHERE mail=$1 OR username=$1", [req.body.username], function(err, result) {
    if (err) {
      console.log(err);
      res.status(500);
    } else {
      console.log(result.rows);
      if (!result.rows.length || result.rows[0].password != req.body.password) {
        console.log('a');
        res.sendStatus(401);
      } else {
        //res.header('auth-token', token).send(token);
        res.status(200).json({token: result.rows[0].token});
        return;
      }
    }
  });
  // if (!res.headersSent)
  //   res.sendStatus(401);
});

router.get('/invite', async function(req, res) {
  if (!req.headers.authorization) {
    res.sendStatus(401);
    return;
  }
  var token = req.headers.authorization.substr(7);
  var id = await getUserIdFromToken(token);
  if (!id) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.board_token || !req.query.mail) {
    res.sendStatus(400);
    return;
  }
  id = await getUserIdFromMail(req.query.mail);
  if (!id) {
    res.sendStatus(400);
    return;
  }
  db.query('UPDATE boards SET auth_users = array_append(auth_users, $1) WHERE token=$2', [id, req.query.board_token], function(err, result2) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      console.log(result2);
      res.sendStatus(200);
    }
  });
});

router.delete('/uninvite', async function(req, res) {
  if (!req.headers.authorization) {
    res.sendStatus(401);
    return;
  }
  var token = req.headers.authorization.substr(7);
  var id = await getUserIdFromToken(token);
  if (!id) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.board_token || !req.query.mail) {
    console.log('a');
    res.sendStatus(400);
    return;
  }
  id = await getUserIdFromMail(req.query.mail);
  if (!id) {
    res.sendStatus(400);
    return;
  } else {
    var result = await db.query('SELECT * FROM boards WHERE token=$1', [req.query.board_token]);
    if (!result.rows.length) {
      res.sendStatus(400);
      return;
    }
    var i = 0;
    while (i < result.rows[0].auth_users.length) {
      if (result.rows[0].auth_users[i] == id)
        break;
      i = i + 1;
    }
    if (i == result.rows[0].auth_users.length) {
      res.sendStatus(204);
      return;
    }
    result.rows[0].auth_users.splice(i, 1);
  }
  db.query('UPDATE boards SET auth_users=$1 WHERE token=$2', [result.rows[0].auth_users, req.query.board_token], function(err) {
    if (err) {
      console.log(err)
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
})

async function getUserIdFromToken(token) {
  var res = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!res.rows.length)
    return (0);
  return (res.rows[0].id);
}

async function getUserIdFromMail(mail) {
  var res = await db.query('SELECT * FROM users WHERE mail=$1', [mail]);
  if (!res.rows.length)
    return (0);
  return (res.rows[0].id);
}

module.exports = router;
