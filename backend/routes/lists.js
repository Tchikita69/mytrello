var express = require('express');
var router = express.Router();
var db = require('../dbconf');

router.post('/create', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 256 || req.body.name.length <= 0 || !req.body.board_token) {
      res.sendStatus(400);
      return;
    }
    console.log(req.body.board_token);
    db.query('INSERT INTO lists(name) VALUES ($1) RETURNING id', [req.body.name], function(err, result2) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        db.query('UPDATE boards SET lists = array_append(lists, $1) WHERE token=$2', [result2.rows[0].id, req.body.board_token], function(err) {
          if (err) {
            console.log(err);
            res.sendStatus(500);
          } else if (!res.headersSent)
            res.status(201).json({list_id: result2.rows[0].id});
        });
      }
    });
  });
});

router.post('/update', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (req.body.name.length > 100 || req.body.name.length <= 0 || !req.body.id)
      res.sendStatus(400);
    if (res.headersSent)
      return;
    var q = 'UPDATE lists SET name=$1 WHERE id=$2';
    var val = [req.body.name, req.body.id];
    if (req.body.cards) {
      q = 'UPDATE lists SET name=$1, cards=$2 WHERE id=$3'
      val = [req.body.name, req.body.cards.split(','), req.body.id];
    }
    db.query(q, val, function(err) {
      if (err) {
        console.log(err);
        res.sendStatus(500);
      } else {
        res.sendStatus(200);
      }
    });
  });
});

router.delete('/remove', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var result = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!result.rows.length) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.id) {
    res.sendStatus(400);
    return;
  }
  db.query('DELETE FROM lists * WHERE id=$1', [req.query.id], function(err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
});

module.exports = router;
