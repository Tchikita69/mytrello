var express = require('express');
var router = express.Router();
var db = require('../dbconf');

router.post('/create', async function(req, res) {
  if (!req.headers.authorization) {
    res.sendStatus(401);
    return;
  }
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], async function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else if (!result.rows.length) {
      res.sendStatus(401);
    }
    if (req.body.name.length > 256 || req.body.name.length <= 0)
      res.sendStatus(400);
    if (res.headersSent)
        return;
    var boardToken = genBoardToken(8);
    var r = await db.query('SELECT token from boards', []);
    var tokens = r.rows;
    while (tokenExists(tokens, boardToken)) {
      boardToken = genBoardToken(8);
    }
    db.query('INSERT INTO boards(auth_users, name, token) VALUES ($1, $2, $3)', [[result.rows[0].id], req.body.name, boardToken], function(err) {
      if (err) {
          console.log(err);
          res.sendStatus(500);
      } else {
          res.status(201).json({board_token: boardToken});
      }
    });
  });
});

router.get('/get', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  db.query('SELECT * FROM users WHERE token=$1', [token], async function(err, result) {
    if (err) {
      console.log(err)
      res.sendStatus(500);
      return;
    } else if (!result.rows.length) {
      res.sendStatus(401);
      return;
    }
    if (!req.query.all && !req.query.token) {
      res.sendStatus(400);
      return;
    }
    var query = 'SELECT * FROM boards WHERE auth_users@>$1';
    var values = [];
    values.push([await getUserIdFromToken(token)]);
    console.log(values);
    if (req.query.token) {
      query = 'SELECT * FROM boards WHERE auth_users@>$1 AND token=$2';
      values.push(req.query.token)
    }
    db.query(query, values, async function(err, result2) {
      if (err) {
        console.log(err)
        res.sendStatus(500);
        return;
      } else if (!result2.rows.length) {
        res.sendStatus(404);
        return;
      } else {
        var data = await cleanBoards(result2.rows);
        res.status(200).json({data: data});
      }
    });
  });
});

router.delete('/remove', async function(req, res) {
  var token = req.headers.authorization.substr(7);
  var result = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  if (!result.rows.length) {
    res.sendStatus(401);
    return;
  }
  if (!req.query.token) {
    res.sendStatus(400);
    return;
  }
  db.query('DELETE FROM boards * WHERE token=$1', [req.query.token], function(err) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
    } else {
      res.sendStatus(204);
    }
  });
});

function genBoardToken(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

function tokenExists(tokens, token) {
  var i = 0;
  while (i < tokens.length) {
    if (tokens[i] == token)
      return (true);
    i = i + 1;
  }
  return (false);
}

async function getUserIdFromToken(token) {
  var res = await db.query('SELECT * FROM users WHERE token=$1', [token]);
  return (res.rows[0].id);
}

async function cleanBoards(boards) {
  var res = [];
  for (var i = 0; i < boards.length; i = i + 1) {
    res.push({id: boards[i].id, name: boards[i].name, token: boards[i].token, labels: await getLabels(boards[i].labels), lists: await getLists(boards[i].lists), users: boards[i].auth_users});
  }
  return (res);
}

async function getLabels(label_ids) {
  if (!label_ids || !label_ids.length)
    return ([]);
  var result = await db.query('SELECT * FROM labels WHERE id = ANY($1::int[])', [label_ids]);
  return (result.rows);
}

async function getLists(lists_ids) {
  if (!lists_ids || !lists_ids.length)
    return ([]);
  var result = await db.query('SELECT * FROM lists WHERE id = ANY($1::int[])', [lists_ids]);
  var lists = result.rows;
  var i = 0;
  while (i < result.rows.length) {
    var cards = await db.query('SELECT * FROM cards WHERE id = ANY($1::int[])', [lists[i].cards]);
    lists[i].cards = cards.rows;
    i = i + 1;
  }
  return (lists);
}

module.exports = router;