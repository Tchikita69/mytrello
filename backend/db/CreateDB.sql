-- CREATE DATABASE trello_data;
-- CREATE USER adm WITH PASSWORD 'root';
-- GRANT ALL PRIVILEGES ON DATABASE trello_data TO trello;
-- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO trello;
-- GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO trello;

CREATE TABLE users(
    id serial PRIMARY KEY,
    mail VARCHAR (100) NOT NULL,
    username VARCHAR (100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    token TEXT
);

CREATE TABLE boards(
    id serial PRIMARY KEY,
    token VARCHAR (10) NOT NULL,
    auth_users INTEGER[],
    name VARCHAR (256) NOT NULL,
    labels INTEGER[],
    lists INTEGER[]
);

CREATE TABLE lists(
    id serial PRIMARY KEY,
    name VARCHAR(256) NOT NULL,
    cards INTEGER[]
);

CREATE TABLE cards(
    id serial PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description TEXT,
    members INTEGER[],
    labels INTEGER[],
    checklists INTEGER[]
);

CREATE TABLE labels(
    id serial PRIMARY KEY,
    name VARCHAR (100),
    color VARCHAR (24)
);

CREATE TABLE checklists(
    id serial PRIMARY KEY,
    name VARCHAR(100),
    elems VARCHAR(256)[],
    checked INTEGER[]
);