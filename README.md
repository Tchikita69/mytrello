# Trello

Yearend project that consists on a trello clone, working on web and phone.

### Installation

 Use yarn for the front

```sh
$ cd frontweb
$ yarn install
$ yarn start
```

Now frontweb is running on this address:

```sh
127.0.0.1:3000
```