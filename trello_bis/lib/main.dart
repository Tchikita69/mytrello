import 'package:flutter/material.dart';
import 'package:trello_bis/widgets/drop_down_widget.dart';
import 'package:trello_bis/widgets/side_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Trello Reborn',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
          primaryColor: Color.fromRGBO(58, 66, 86, 1.0)
      ),
      home: MyHomePage(title: 'Accueil'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var widgets = <Widget>[];
  final List<String> _dropdownValues = [
    "Edit",
    "Delete"
  ]; //The list of values we want on the dropdown
  String _currentlySelected = "";
  bool editMode = false;

  Widget dropDownWidget() {
    return DropdownButton(
      //map each value from the lIst to our dropdownMenuItem widget
      items: _dropdownValues
          .map((value) =>
          DropdownMenuItem(
            child: Text(value),
            value: value,
          ))
          .toList(),
      onChanged: (String value) {
        //once dropdown changes, update the state of out currentValue
        setState(() {
          if (value == "Edit")
          editMode = !editMode;
        });
      },
      //this wont make dropdown expanded and fill the horizontal space
      isExpanded: false,
      //make default value of dropdown the first value of our list
      value: _dropdownValues.first,
    );
  }

  void _addArray() {
    setState(() {
      widgets.add(new SwipeList());/*Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.album, size: 50),
              title: Text('Add title'),
              subtitle: Text('Add a description...'),
            ),
          ],
        ),
      ))*/
          /*Container(
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              color: Color.fromRGBO(58, 66, 86, 1.0),
              elevation: 10,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  AppBar(
                    leading: Icon(Icons.album, size: 40),
                    title: TextField(
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Enter a title...',
                      ),
                      style: TextStyle(color: Colors.white),
//                      onChanged: ,
                    ),
                    actions: <Widget>[
                      dropDownWidget()
                    ],
                  ),
                  ButtonBar(
                      children: <Widget>[
                        *//*FlatButton(
                          child: const Text('Edit', style: TextStyle(color: Colors.white)),
                          onPressed: () {},
                        ),*//*
                        RaisedButton(
                          child: Text(editMode ? "Save" : "Edit"),
                          onPressed: () {
                            setState(() {
                              if (editMode == true)
                                editMode = false;
                              else
                                editMode = true;
                            });
                          },
                        ),
                        FlatButton(
                          child: const Text('Delete', style: TextStyle(color: Colors.white)),
                          onPressed: () {},
                        ),
                      ],
                    ),
                ],
              ),
            ),
          ));*/
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        backgroundColor: Colors.blueGrey,
        drawer: NavDrawer(),
        body: new Center(
          child: SingleChildScrollView(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: widgets,
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _addArray,
          tooltip: 'Add Array',
          backgroundColor: Color.fromRGBO(58, 66, 86, 1.0),
          child: Icon(Icons.add),
        ),
    );
  }
}
