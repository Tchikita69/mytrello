import 'package:flutter/material.dart';

class CardArray {

  final String title;
  final String description;
  final ListView cardList;

  const CardArray(
      {
        this.title,
        this.description,
        this.cardList
      });
}