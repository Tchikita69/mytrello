import React, {Component} from "react";
import {Button, withStyles, TextField, IconButton} from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

import { connect } from "react-redux";
import { addList, addCard } from "../actions";
import axios from "axios";

const AddCardButton = withStyles({
    root: {
      boxShadow: 'none',
      textTransform: 'none',
      fontSize: 16,
      padding: '6px 12px',
      lineHeight: 1.5,
    },
  })(Button);

const RenderFormButton = withStyles({
    root: {
        backgroundColor: "#5aac44",
        color: "white", 
        textTransform: 'none',
        '&:hover': {
            backgroundColor: '#0069d9',
            borderColor: '#0062cc',
            boxShadow: 'none',
          },
    },
  })(Button);

class TrelloAction extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false,
            text: "",
        }
        this._handleTextChange = this._handleTextChange.bind(this);
    }

    _handleTextChange(e) {
        this.setState({text: e.target.value})
    }

    addListRequest(text) {
        const token = JSON.parse(localStorage.getItem("trellotokens"));
        const boardtoken = JSON.parse(localStorage.getItem("board_token"));
  
        console.log("i swear it works")
        // axios(
        //   { 
        //     method: 'POST',
        //     url: 'http://127.0.0.1:4242/lists/create',
        //     headers: {
        //       "Authorization": "bearer " + token
        //     },
        //     data: {
        //       name: text,
        //       board_token: boardtoken
        //     }
        //   })
        //   .then((res) => {
        //       console.log("update list successfully")
        //   }, (err) => {
        //     console.log(err);
        //     alert(err)
        //   });
    }

    addCard(text) {
        const token = JSON.parse(localStorage.getItem("trellotokens"));
        const boardtoken = JSON.parse(localStorage.getItem("board_token"));
    
        axios(
          { 
            method: 'POST',
            url: 'http://127.0.0.1:4242/cards/create',
            headers: {
              "Authorization": "bearer " + token
            },
            data: {
              name: text,
              board_token: boardtoken
            }
          })
          .then((res) => {
              console.log("update list successfully")
          }, (err) => {
            console.log(err);
            alert(err)
          });
    }

    _handleAddList() {
        const { dispatch } = this.props;
        const { text } = this.state;

        if (text) {
            dispatch(addList(text))
            this.addListRequest(text)
            this.setState({text: "", open: false})
        }
        return
    }

    _handleAddCard() {
        const { dispatch, listId } = this.props;
        const { text } = this.state;

        if (text) {
            dispatch(addCard(listId, text))
            this.setState({text: "", open: false})
        }
    }

    renderForm() {
        const {list } = this.props;
        const text1 = list ? "Enter list title" : "Enter card content";
        const text2 = list ? "Add list" : "Add card";

        return (
            <div style={{marginLeft: 7}}>
            <TextField id="standard-basic" label={text1} multiline 
            value={this.state.text} onChange={this._handleTextChange}/>
                <div style={{marginTop: 10, minWidth: 250}}>
                    <RenderFormButton
                    onClick={() => {list ? this._handleAddList() : this._handleAddCard()}}>
                        {text2}
                    </RenderFormButton>

                    <IconButton aria-label="delete" size="small" 
                    style={{marginLeft: 20}} onClick={() => {this.setState({open: false})}}>
                        <CloseIcon />
                    </IconButton>
                </div>
            </div>
        )
    }

    renderAdd() {
        const { list } = this.props;
        const text = list ? "Add another list" : "Add another card"
        
        return (
            <div style={{marginTop: 10, marginLeft: 7, minWidth: 250, height: "100%"}}>
            <AddCardButton variant="contained" 
            color="default"
            startIcon={<AddIcon />}
            onClick={() => {this.setState({open: true})}}
            >
            {text}
            </AddCardButton>
            </div>
        )
    }

    render() {
        return this.state.open ? this.renderForm() : this.renderAdd()
    }
}

export default connect()(TrelloAction);