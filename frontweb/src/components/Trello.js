import React, {Component} from "react";
import TrelloList from "./TrelloList";
import { connect } from "react-redux";
import TrelloAction from './TrelloAction';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { sortList } from '../actions';
import styled from "styled-components";
import axios from "axios";

import TrelloInside from "./TrelloInside";

import { initList } from "../actions";

const ListContainer = styled.div`
  display: flex;
  flex-direction: row;
` 

class Trello extends Component {
    constructor(props) {
        super(props)
        this.state = {
          boards: [],
          lists: [],
        }
        this.onDragEnd = this.onDragEnd.bind(this)
        // this.loadBoard()
    }

    loadLists() {
      const { dispatch } = this.props

      this.state.lists.map(list => {
        console.log("list: " + list)
        console.log(list.id, list.name, list.cards)
        dispatch(initList(list.id, list.name, []))
        return null
      })
    }

    loadBoard() {
      const token = JSON.parse(localStorage.getItem("trellotokens"));
      const boardtoken = JSON.parse(localStorage.getItem("board_token"));
  
      axios(
        { 
          method: 'GET',
          url: 'http://127.0.0.1:4242/boards/get',
          headers: {
            "Authorization": "bearer " + token
          },
          params: {
            all: false,
            token: boardtoken
          }
        })
        .then((res) => {
          console.log("Load board: [success]")
          console.log(res.data.data[0].lists)
          this.setState({lists: res.data.data[0].lists})
          this.loadLists()
        }, (err) => {
          console.log(err);
          alert(err)
        });
    }

    componentDidUpdate() {
        console.log("State updated:\t" + this.props.lists)
    }
    
    onDragEnd(res) {
        const {destination, source, draggableId, type} = res;
    
        if (!destination) {
          return 
        }
        this.props.dispatch(sortList(source.droppableId, 
          destination.droppableId, 
          source.index,
          destination.index,
          draggableId, type))
    }

    render() {
        const { lists, openCard } = this.props

        return(
          <div>
          <DragDropContext onDragEnd={this.onDragEnd}>
            <div>
              <Droppable droppableId="all-lists" direction="horizontal" type="list">
                {provided => (
                  
                    <ListContainer {...provided.droppableProps} ref={provided.innerRef}>
                      {/* Some requests here */}
                        {lists.map((list, index) => <TrelloList 
                        key={list.id} listId={list.id} title={list.title} cards={list.cards} index={index} />)}
                        {provided.placeholder}
                        <TrelloAction list/>
                    </ListContainer>
                )}
              </Droppable>
            </div>
            {openCard.open_state ? <TrelloInside openCard={openCard}/> : null}
          </DragDropContext>
          </div>
        )
      }
}

const mapStateToProps = state => {
    return {
      board: state.board,
      lists: state.lists,
      openCard: state.card,
    }
};

export default connect(mapStateToProps)(Trello);