import React, { useState } from 'react';

// MY COMPONENTS IMPORT
import Trello from "./Trello";
import Authentification from "./Authentification";
import ChooseBoard from "./ChooseBoard";

// ROUTES  and CONTEXT
import PrivateRoute from "./PrivateRoute";
import {BrowserRouter, Route, Switch, Redirect} from "react-router-dom";
import { AuthContext } from "../context/auth";

// LAYOUTS
import Layout from "./layout/DefaultLayout";

function App(props) {
  const existingTokens = JSON.parse(localStorage.getItem("trellotokens"));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = (data) => {
    localStorage.setItem("trellotokens", JSON.stringify(data));
    setAuthTokens(data);
  }

  return (
    <AuthContext.Provider value={{authTokens, setAuthTokens:setTokens}}>
      <BrowserRouter>
        <Switch>
          <Route path="/auth" component={Authentification}/>
          <Route path="/trello" component={Trello} layout={Layout}/>
          <Route test="voilà" path="/choose" component={ChooseBoard} layout={Layout}/>
          <Redirect exact from="/" to="/auth"/>
        </Switch>
      </BrowserRouter>
    </AuthContext.Provider>
  )
}

export default App;