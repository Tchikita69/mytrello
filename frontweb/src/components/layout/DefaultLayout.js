import React from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import InfoIcon from '@material-ui/icons/Info';
import SettingsIcon from '@material-ui/icons/Settings';
import PanoramaIcon from '@material-ui/icons/Panorama';
import SearchIcon from '@material-ui/icons/Search';
import HelpIcon from '@material-ui/icons/Help';

import PrimarySearchAppBar from './Projectbar';
import { Redirect } from "react-router-dom";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: drawerWidth,
  },
  title: {
    flexGrow: 1,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: 'flex-start',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    marginTop: theme.spacing(5),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginRight: 0,
  },
}));

export default function PersistentDrawerRight({children}) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [logout, setLogout ] = React.useState(false);
  const scale = 8;
  const width = 885;
  const height = 272;

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleIcons = (index) => {
      switch (index) {
          case 0: {
            return <MeetingRoomIcon/>
          } case 1: {
              return <InfoIcon/>
          } case 2: {
              return <SearchIcon/>
          } case 3: {
              return <SettingsIcon/>
          } case 4: {
              return <PanoramaIcon/>
          } default: {
              return <HelpIcon/>
          }
      }
  }

  if (logout) {
    return <Redirect to="/auth"/>;
  }
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
        <div className={classes.title}>
                <img src="https://d2k1ftgv7pobq7.cloudfront.net/meta/u/res/images/trello-header-logos/af7af6ed478d3460709d715d9b3f74a4/trello-logo-white.svg" 
                alt="Logo" 
                width={width / scale} height={height / scale}/>
        </div>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="end"
            onClick={handleDrawerOpen}
            className={clsx(open && classes.hide)}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
        {/* <Toolbar style={{backgroundColor: "rgba(255, 255, 255, 0.4)"}}>
        <div className={classes.title}>
                <Typography>Some random project</Typography>
        </div>
        </Toolbar> */}
        {children.props.path === "/trello" ? <PrimarySearchAppBar/> : null}
      </AppBar>
      <main
      id="background"
      style={{height:"874px", backgroundSize:"cover"}}
        className={clsx(classes.content, {
          [classes.contentShift]: open,
        })}
      >
        {children.props.path === "/trello" ? <div className={classes.drawerHeader}/> : null}
        {children}
      </main>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="right"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.drawerHeader}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            <Typography>Menu</Typography>
          </IconButton>
        </div>
        <Divider />
        <List>
          {['Logout', 'About this board', 'Search cards', 'Settings', 'Change background'].map((text, index) => (
            <ListItem button key={text} onClick={() => {
              if (index === 0) {
                localStorage.removeItem("trellotokens")
                localStorage.removeItem("board_token")
                setLogout(true)
              }
            }}>
              <ListItemIcon>{handleIcons(index)}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        {/*<select id="selectback" onChange={() => document.getElementById("background").style.backgroundImage = "url(" + document.getElementById("selectback").value +")"}>
          <option value="">default</option>
          <option value="https://www.travelercar.com/wp-content/uploads/2016/04/4a36e314016aa914f203ea6b7d579dc6_large.jpeg">mer</option>
          <option value="https://www.naturepaysage.photo/wp-content/uploads/Nature-Paysage-portfolio-27-octobre-2019-0026.jpg">lac</option>
          </select>*/}
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://leonard.vinci.com/wp-content/uploads/2018/09/joseph-chan-271665-unsplash.jpg)"}
        src="https://leonard.vinci.com/wp-content/uploads/2018/09/joseph-chan-271665-unsplash.jpg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://www.travelercar.com/wp-content/uploads/2016/04/4a36e314016aa914f203ea6b7d579dc6_large.jpeg)"}
        src="https://www.travelercar.com/wp-content/uploads/2016/04/4a36e314016aa914f203ea6b7d579dc6_large.jpeg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://www.naturepaysage.photo/wp-content/uploads/Nature-Paysage-portfolio-27-octobre-2019-0026.jpg)"}
        src="https://www.naturepaysage.photo/wp-content/uploads/Nature-Paysage-portfolio-27-octobre-2019-0026.jpg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://s2.best-wallpaper.net/wallpaper/1600x900/1406/Bamboo-forest-green-nature-landscape_1600x900.jpg)"}
        src="https://s2.best-wallpaper.net/wallpaper/1600x900/1406/Bamboo-forest-green-nature-landscape_1600x900.jpg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://dailygeekshow.com/wp-content/uploads/2020/02/galaxie.jpg)"}
        src="https://dailygeekshow.com/wp-content/uploads/2020/02/galaxie.jpg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://media.routard.com/image/85/6/plongee-ile-palmes.1521856.jpeg)"}
        src="https://media.routard.com/image/85/6/plongee-ile-palmes.1521856.jpeg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://www.ateliersduvoyage.com/wp-content/uploads/2019/03/VWIDMFs2Aj1UYlAuADEEJgRmB39VaQAuCTI.jpg)"}
        src="https://www.ateliersduvoyage.com/wp-content/uploads/2019/03/VWIDMFs2Aj1UYlAuADEEJgRmB39VaQAuCTI.jpg" style={{height:"140px"}}/>
        <img alt="" onClick={() => document.getElementById("background").style.backgroundImage = "url(https://static.nationalgeographic.fr/files/styles/image_3200/public/pangolin-cites-03.ngsversion.1501687833883.adapt_.1900.1.jpg)"}
        src="https://static.nationalgeographic.fr/files/styles/image_3200/public/pangolin-cites-03.ngsversion.1501687833883.adapt_.1900.1.jpg" style={{height:"140px"}}/>
        <Divider />
        {/* <List>
          {['All mail', 'Trash', 'Spam'].map((text, index) => (
            <ListItem button key={text}>
              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List> */}
      </Drawer>
    </div>
  );
}