import React, {Component} from "react";
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
// import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Draggable } from 'react-beautiful-dnd';

import { connect } from "react-redux";
import { openCard } from "../actions";

class TrelloCard extends Component {
    openCard() {
        const {id, content, description, dispatch, listname, listId } = this.props

        dispatch(openCard(id, true, content, description, listname, listId))
    }

    render () {
        return(
            <div>
            <Draggable draggableId={String(this.props.id)} index={this.props.index}>
                {(provided) => (
                    <div ref={provided.innerRef} {...provided.draggableProps} 
                    {...provided.dragHandleProps}>
                    <Card style={{marginBottom: 5}} onClick={() => 
                        {this.openCard()}}>
                        <CardContent>
                            <Typography variant="body2">{this.props.content}</Typography>
                        </CardContent>
                    </Card>
                    </div>
                )}
            </Draggable>
            </div>
        )
    }
}

export default connect()(TrelloCard);