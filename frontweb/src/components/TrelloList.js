import React, {Component} from "react";
import TrelloCard from "./TrelloCard";
import TrelloAction from "./TrelloAction";
import { Droppable, Draggable } from 'react-beautiful-dnd';

import styled from "styled-components";
import { Typography } from "@material-ui/core";

const ListContainer = styled.div`
background-color: #dfe3e6;
padding: 1em;
border-radius: 4px;
height: 100%;
width: 250px;
min-width: 250px;
margin-right: 2em;
` 

class TrelloList extends Component {
    render () {
        return(
            <Draggable draggableId={String("list-" + this.props.listId)} index={this.props.index}>
                {                
                    provided => (
                        <ListContainer {...provided.draggableProps} 
                        ref={provided.innerRef} {...provided.dragHandleProps}>   
                        <Droppable droppableId={String(this.props.listId)}>
                        {provided => (
                            <div {...provided.draggableProps} 
                            ref={provided.innerRef}>
                                <div style={{marginTop: 10, marginBottom: 20, paddingLeft: 10}}>
                                <Typography gutterBottom
                                style={{color: "#213455", fontFamily: "Roboto", fontWeight: "bold", 
                                }}>{this.props.title}</Typography>
                                </div>
                                {this.props.cards.map((card, index) => 
                                    <TrelloCard key={card.id}
                                                id={card.id} 
                                                index={index} 
                                                content={card.content}
                                                listname={this.props.title}
                                                listId={this.props.listId}
                                                description={card.description}
                                    />)}
                                {provided.placeholder}
                                <TrelloAction listId={this.props.listId}/>
                            </div>
                        )}
                        </Droppable>
                        </ListContainer>
                     )}  
             </Draggable>   
        )
    }
}

// const styles = {
//     list: {
//         backgroundColor: "#ccc",
//         padding: 7,
//         borderRadius: 3,
//         height: "100%",
//         width: 250,
//         minWidth: 250,
//         margin: 7,
//     }
// }

 export default TrelloList;