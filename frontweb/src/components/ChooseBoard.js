import React, { Component } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { withStyles } from '@material-ui/core/'
import Divider from '@material-ui/core/Divider';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import ArrowRightIcon from '@material-ui/icons/ArrowRight';

import { Redirect } from "react-router-dom";

import { connect } from "react-redux";
import { loadBoard, clearBoard } from "../actions";

const StyledListItem = withStyles({
  root: {
    backgroundColor: "#C4D6D4 ",
  },
})(ListItem);

class ChooseBoard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            boards: [],
            board_input: "",
            load_board: false,
        }
        this.getAllBoards()
        this._handleBoardChange = this._handleBoardChange.bind(this)
    }

    _handleBoardChange(e) {
      this.setState({board_input: e.target.value})
    }

    generateBoard(clickedBoard) {
      const { dispatch } = this.props;
      dispatch(loadBoard(clickedBoard.name, 
                        clickedBoard.id, 
                        clickedBoard.token,
                        clickedBoard.lists))
      localStorage.setItem("board_token", JSON.stringify(clickedBoard.token))
      this.setState({load_board: true})
    }

    createBoard() {
      const token = JSON.parse(localStorage.getItem("trellotokens"));

      axios({ 
          method: 'POST',
          url: 'http://127.0.0.1:4242/boards/create',
          headers: {
            "accept": "application/json",
            "Authorization": "bearer " + token
          },
          data: {
            name: this.state.board_input,
          }
        })
        .then((res) => {
          console.log("Create board [SUCCESS] " + res)
          this.getAllBoards()
        })
        .catch((error)=>{
          console.log(error)
        })
    }

    getAllBoards() {
        const token = JSON.parse(localStorage.getItem("trellotokens"));
  
        axios(
          { 
            method: 'GET',
            url: 'http://127.0.0.1:4242/boards/get',
            headers: {
              "Authorization": "bearer " + token
            },
            params: {
              all: true
            }
          })
          .then((res) => {
            console.log("Get all boards: [success]")
            console.log(res.data.data)
            this.setState({boards: res.data.data})
          }, (err) => {
            console.log(err);
            alert(err)
          });
    }

    render() {
        const {dispatch } = this.props
        if (this.state.load_board) {
          dispatch(clearBoard())
          return <Redirect to="/trello"/>;
        }
        return(
            <Grid
                container
                spacing={0}
                direction="column"
                alignItems="center"
                style={{marginTop: "5%"}}
            >
                <Grid item xs={3}>
                    <h1>Choose the board in which you want to work ... </h1>
                    <div style={{width: '100%', maxWidth: 360, backgroundColor: "#dfe3e6", marginTop: 30, marginBottom: 30}}>
                    <List>
                        {this.state.boards.map(board => {
                            return(
                              <div key={board.id} >
                                <StyledListItem button key={board.id} onClick={() => {this.generateBoard(board)}}>
                                  <ListItemIcon> <ArrowRightIcon/> </ListItemIcon>
                                  <ListItemText primary={board.name}/>
                                </StyledListItem>
                                <Divider/>
                              </div> 
                            )
                        })}
                    </List>
                    </div>
                    <div style={{marginTop: 60}}>
                    <h3>...or create a new one right here</h3>
                    <div>
                      <TextField label="Enter a board name" value={this.state.board_input} onChange={this._handleBoardChange}/>
                    </div>
                    <div style={{marginTop: 20}}>
                    <Button color="primary" onClick={() => {this.createBoard()}}>Create board</Button>
                    </div>
                    </div>
                </Grid>   
            </Grid> 
        )
    }
}

export default connect()(ChooseBoard);