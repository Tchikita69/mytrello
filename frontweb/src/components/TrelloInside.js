import { withStyles } from '@material-ui/core/styles';

import React, { Component } from "react";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box'
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MuiDialogActions from '@material-ui/core/DialogActions';
import { openCard, editDescription } from "../actions";
import { connect } from "react-redux";

// Notification system
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';


toast.configure()

const styles = (theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(2),
      width: 600,
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });
  
  const DialogContent = withStyles((theme) => ({
    root: {
      padding: theme.spacing(2),
    },
  }))(MuiDialogContent);
  
  const DialogActions = withStyles((theme) => ({
    root: {
      margin: 0,
      padding: theme.spacing(1),
    },
  }))(MuiDialogActions);
  
  
  const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        <Typography variant="h6">{children}</Typography>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

class TrelloInside extends Component {
    constructor(props) {
        super(props)
        this.state = {
          description: this.props.openCard.description,
        }
        this.closewindow = this.closewindow.bind(this)
        this._handleDescChange = this._handleDescChange.bind(this)
        this.changeDescription = this.changeDescription.bind(this)
    }

    _handleDescChange(e) {
      this.setState({description: e.target.value})
    }

    closewindow() {
      const { dispatch } = this.props;

      dispatch(openCard(null, false, null, null))
    }

    changeDescription() {
      const { dispatch, openCard} = this.props

      if (this.state.description !== "") {
        dispatch(editDescription(openCard.listId, openCard.id, this.state.description))
        toast.info("Modifications saved successfully !", {autoClose: 2000, hideProgressBar: true})
      } else {
        toast.error("Please enter a description", {autoClose: 2000, hideProgressBar: true})
      }
    }

    render() {
        const { openCard } = this.props

        return (
            <Dialog onClose={this.closewindow} aria-labelledby="customized-dialog-title" 
                    open={openCard.open_state ? true : false}>
            <DialogTitle id="customized-dialog-title" onClose={this.closewindow}>
                {openCard.content ? "\"" + openCard.content + "\" in ": null}
                 {openCard.listname ? openCard.listname : null}
            </DialogTitle>
            <DialogContent style={{height: 800}} dividers>
                <Box m={1}>
                    <Typography style={{fontWeight: "bold"}} variant="h6">Description:</Typography>
                </Box>
                <Box m={1}>
                    <TextField fullWidth id="description"
                            placeholder="Placeholder"
                            multiline
                            label="Edit description"
                            variant="filled"
                            value={this.state.description}
                            onChange={this._handleDescChange}
                            // defaultValue={openCard.description}
                    />
                </Box>
            </DialogContent>
            <DialogActions>
              <Box m={1}>
                    <Button onClick={this.changeDescription} variant="contained" color="primary">Save</Button>
                </Box>
            </DialogActions>
            </Dialog>

        )
    }
}

export default connect()(TrelloInside);