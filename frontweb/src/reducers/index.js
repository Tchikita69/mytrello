import {combineReducers} from "redux";
import listsReducer from "./listsReducer";
import openCardReducer from "./openCardReducer";
import currentBoardReducer from "./currentBoardReducer";

export default combineReducers({
    lists: listsReducer,
    card: openCardReducer,
    board: currentBoardReducer,
});