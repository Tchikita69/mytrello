import { CONSTANTS } from "../actions";

const openCard = {
        open_state: false,
        id: null,
        content: null,
        description: null,
        listname: null
}

const openCardReducer = (state = openCard, action) => {
    switch(action.type) {
        case CONSTANTS.OPEN_CARD:
            const {id, open_state, content, description, listname, listId} = action.payload
            const newCard = {
                    open_state: open_state,
                    id: id,
                    content: content,
                    description: description,
                    listname: listname,
                    listId: listId,
                }
            return newCard;
        default:
            return state;
    }    
}
export default openCardReducer;