import { CONSTANTS } from "../actions";

const boardInfos = {
    name: "My board",
    id: "0",
    token: "289",
    lists: []
}

const currentBoardReducer = (state = boardInfos, action) => {
    switch(action.type) {
        case CONSTANTS.LOAD_BOARD:
            const { name, id, token, lists } = action.payload
            const newBoard = {
                name: name,
                id: id,
                token: token,
                lists: lists
            }
            return newBoard;
        default:
            return state;
    }    
}

export default currentBoardReducer;