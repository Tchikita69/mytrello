import { CONSTANTS } from "../actions"

// const initState = [
//     {
//         id: 0,
//         title: "Courses",
//         cards: [
//             {
//                 id: 0,
//                 content: "Acheter du lait",
//                 description: "",
//             },
//             {
//                 id: 1,
//                 content: "Acheter des oeufs",
//                 description: "",
//             }
//         ]
//     },
//     {
//         id: 1,
//         title: "Trello project",
//         cards: [
//             {
//                 id: 2,
//                 content: "Commencer le projet",
//                 description: "",
//             },
//             {
//                 id: 3,
//                 content: "Faire le projet",
//                 description: "",
//             },
//             {
//                 id: 4,
//                 content: "Terminer le projet",
//                 description: "",
//             },
//             {
//                 id: 5,
//                 content: "Avoir une bonne note ?",
//                 description: "gros test",
//             }
//         ]
//     }
// ]


const initState = []

function getNumberofCards(state) {
    let nb = 0;
    state.map(list =>
        nb += Object.keys(list.cards).length
    )
    return nb
}

const listsReducer = (state = initState, action) => {
    switch(action.type) {
        case CONSTANTS.INIT_LIST: {
            const { id, title, cards } = action.payload
            const newList = {
                id: id,
                title: title,
                cards: cards,
            }
            return [...state, newList]
        }
        case CONSTANTS.CLEAR_BOARD: {
            return []
        }
        case CONSTANTS.EDIT_DESCRIPTION: {
            const { listId, cardId, description } = action.payload

            const newState = state.map(list => {
                if (list.id === listId) {
                    return {...list, cards: [...list.cards.map(card => {
                        if (card.id === cardId)
                            return {...card, description: description}
                        return card
                    })]}
                } else {
                    return list
                } 
            })
            return newState
        } case CONSTANTS.ADD_LIST:
            console.log("number of cards" + getNumberofCards(state))
            const newList = {
                id: Object.keys(state).length,
                title: action.payload,
                cards: [],
            }
            return [...state, newList]
        case CONSTANTS.ADD_CARD: {
            const nbOfCards = getNumberofCards(state)
            // Foreach list in my state, check if ID match 
            const newState = state.map(list => {
                if (list.id === action.payload.listId) {
                    const newCard = {
                        id: nbOfCards,
                        content: action.payload.text
                    }
                    // Return a copy of the list with the card object modified
                    return {
                        ...list,
                        cards: [...list.cards, newCard] // concat my newCard with the old ones
                    }
                } else {
                    return list // Do nothing with the list
                }
            }) 
            return newState
        }
        case CONSTANTS.DRAG_SORT:
            const {droppableIdStart, 
            droppableIdEnd, droppableIndexStart, 
            droppableIndexEnd, draggableId, type} = action.payload
            const newState = [...state]

            if (type === "list") {
                const list = newState.splice(droppableIndexStart, 1)
                console.log(newState)
                newState.splice(droppableIndexEnd, 0, ...list)
                console.log(newState)
                return newState
            }
            if (droppableIdStart === droppableIdEnd) {
                console.log(droppableIdStart, 
                    droppableIdEnd, droppableIndexStart, 
                    droppableIndexEnd, draggableId)
                const list = state.find(list => String(list.id) === droppableIdEnd)
                const card = list.cards.splice(droppableIndexStart, 1)
                list.cards.splice(droppableIndexEnd, 0, ...card)
            }
            if (droppableIdStart !== droppableIdEnd) {
                const listCard = state.find(list => String(list.id) === droppableIdStart)
                const card = listCard.cards.splice(droppableIndexStart, 1)
                const listEnd = state.find(list => String(list.id) === droppableIdEnd)

                listEnd.cards.splice(droppableIndexEnd, 0, ...card)
            }
            return newState
        default:
            return state;
    }
}

export default listsReducer;
