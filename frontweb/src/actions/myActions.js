import { CONSTANTS} from '../actions'

export const initList = (id, title, cards) => {
    return {
        type: CONSTANTS.INIT_LIST,
        payload: { id, title, cards }
    }
}

export const addList = (title) => {
    return {
        type: CONSTANTS.ADD_LIST,
        payload: title
    }
}

export const addCard = (listId, text) => {
    return {
        type: CONSTANTS.ADD_CARD,
        payload: { text, listId }
    }
}

export const openCard = (id, open_state, content, description, listname, listId) => {
    return {
        type: CONSTANTS.OPEN_CARD,
        payload: { open_state, id, content, description, listname, listId } 
    }
}

export const editDescription = (listId, cardId, description) => {
    return {
        type: CONSTANTS.EDIT_DESCRIPTION,
        payload: { listId, cardId, description}
    }
}

export const sortList = (droppableIdStart, 
    droppableIdEnd, droppableIndexStart, 
    droppableIndexEnd, draggableId, type) => {
    return {
        type: CONSTANTS.DRAG_SORT,
        payload: {
            droppableIdStart, 
            droppableIdEnd, droppableIndexStart, 
            droppableIndexEnd, draggableId, type
        }
    }
}

export const loadBoard = (name, id, token, lists) => {
    return {
        type: CONSTANTS.LOAD_BOARD,
        payload: {
            name, id, token, lists
        }
    }
}

export const clearBoard = () => {
    return {
        type: CONSTANTS.CLEAR_BOARD,
    }
}