import React, {Component} from 'react';
import Board from './components/Board';
import Card from './components/Card';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
/*import logo from './logo.svg';*/
import axios from 'axios';
import './main.css';

export default class App extends Component {
  state = {
    login:0
  }
  authentificate(email, password) {
    axios(
      { 
        method: 'post',
        url: 'http://127.0.0.1:4242/users/login',
        data: {
          email: email,
          password: password
        }
      })
      .then((res) => {
    this.setState({login: 3});
    this.setState({token: res.data.user});
      alert("Vous êtes connecté.");
      console.log(res.data.user);
    }, (err) => {
      console.log(err);
      alert(err)
    });
  }

  register(email, username, password) {
    axios(
      { 
        method: 'post',
        url: 'http://127.0.0.1:4242/users/register',
        data: {
          mail: email,
          username: username,
          password: password
        }
      })
      .then((res) => {
    this.setState({login: 0});
    this.setState({token: res.data.user});
      alert("Félicitations " + email + ", votre compte a été créé !")
      console.log(res.data.user);
    }, (err) => {
      console.log(err);
      alert(err)
    });
  }

  render () {
  if (this.state.login == 3) {
  return (
    <div className="App">
      <header className="App-header">
        <main className="flexbox">
          <Board id="board-1" className="board">
            To do
            <Card id="card-1" className="card" draggable="true">
              <p>Card one</p>
            </Card>
          </Board>
          <Board id="board-2" className="board">
            Doing
            <Card id="card-2" className="card" draggable="true">
              <p>Card two</p>
            </Card>
            <Card id="card-3" className="card" draggable="true">
              <p>Card three</p>
            </Card>
          </Board>
          <Board id="board-3" className="board">
            Done
            <Card id="card-4" className="card" draggable="true">
              <p>Card four</p>
            </Card>
            <Card id="card-5" className="card" draggable="true">
              <p>Card five</p>
            </Card>
          </Board>
        </main> 
      </header>
    </div>
  );
  }
  if (this.state.login == 2) {
    return (
      <div className="App">
        <header className="App-header">
        <div className="loginttl">
        <h1 style={{fontSize: "100px"}}>Trello</h1>
        </div>
        <div className="login">
          <div className="case">
          <TextField variant="filled" id="email" label="email"/>
          <TextField variant="filled" id="username" label="username"/>
          <TextField variant="filled" id="password" type="password" label="password"/>
          <Button variant="contained" style={{marginTop:"20px"}} onClick={() => this.register(document.getElementById("email").value, document.getElementById("username").value, document.getElementById("password").value)}><h1>REGISTER</h1></Button>
          </div>
          </div>
      </header>
    </div>
    );
  }
  if (this.state.login == 1) {
    return (
      <div className="App">
        <header className="App-header">
        <div className="loginttl">
        <h1 style={{fontSize: "100px"}}>Trello</h1>
        </div>
        <div className="login">
          <div className="case">
          <TextField variant="filled" id="email" label="email"/>
          <TextField variant="filled" id="password" type="password" label="password"/>
          <Button variant="contained" style={{marginTop:"20px"}} onClick={() => this.authentificate(document.getElementById("email").value, document.getElementById("password").value)}><h1>LOGIN</h1></Button>
          </div>
          </div>
      </header>
    </div>
    );
  }
  if (this.state.login == 0) {
    return (
    <div className="App">
      <header className="App-header">
      <div className="loginttl">
      <h1 style={{fontSize: "100px"}}>Trello</h1>
      </div>
      <div className="login">
        <div className="case">
        <Button variant="contained" onClick={() => this.setState({login: 1})}><h1>LOGIN</h1></Button>
        <Button variant="contained" style={{marginTop:"20px"}} onClick={() => this.setState({login: 2})}><h1>REGISTER</h1></Button>
        </div>
        </div>
      </header>
    </div>
    );}
    }
}

//export default App;
